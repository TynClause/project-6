var context = '';
function processNewMessage(text) {
    if (context === '') {
        if (text === 'hi' || text === 'hello' || text === 'Hi' || text === 'Hello') {
            response = 'Hi there! Do you want to talk about studies or sports?';
        } else if (text === 'studies') {
            context = 'studies';
            response = 'Ok, studies. Do you like studying? ';
        } else if (text === 'sports') {
            context = 'sports';
            response = 'Ok, sports. Do you like sports?';
        }
        else if (text === '') {
            content = 'help';
            response = 'Can i Help you';
        }
        else {
            content = '';
            response = 'Witch one do you like, studies or sports ?';
        }
    } else if (context === 'studies') {
        if (text === 'yes') {
            response = 'That\'s great! What do you study?';
        }
        else {
            response = 'oh... oke';
        }
    } else if (context === 'sports') {
        if (text === 'yes') {
            response = 'What is your favorite team?';
        }
        else {
            response = 'oh... oke';
        }
    } else if (context === 'help' || context === 'help') {
        if (text === 'yes') {
            response = 'sorry,maybe later..';
        }
        else {
            response = 'sorry,maybe later..';
        }
    }


    addBotChatLine(response);
}

function sendChat() {
    var text = document.getElementById("chatbox").value;
    addChatLine(text, false);
    processNewMessage(text);
    document.getElementById("chatbox").value = '';
}

function addBotChatLine(text) {
    addChatLine(text, true);
}

function addChatLine(text, isFromBot) {
    addChatStatusLine(isFromBot);
    var p = document.createElement("p");
    p.className = 'chatline';
    var node = document.createTextNode(text);
    p.appendChild(node);
    document.getElementById("user").appendChild(p);
    window.scrollTo(0, document.body.scrollHeight);
}

function addChatStatusLine(isFromBot) {
    var p = document.createElement("p");
    p.className = (isFromBot) ? 'statusline status-bot' : 'statusline status-human';
    var d = new Date();
    var status = ((isFromBot) ? 'BOT' : 'USER') + ' ● ' + d.getHours() + ':' + d.getMinutes();
    var node = document.createTextNode(status);
    p.appendChild(node);
    document.getElementById("user").appendChild(p);
}

function popdown() {
    document.getElementById("cb").style.display = "none";
}

function popup() {
    document.getElementById("cb").style.display = "block";
}