from django.shortcuts import render

# Create your views here.


def home(request):
    response = {'author': 'Elvin Nur Furqon'}
    return render(request, 'base.html', response)
